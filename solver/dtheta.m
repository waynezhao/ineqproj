%------------------------------------------------------------------------------
%
% Evaluate the derivative of the line search function theta at the point lambda.
% This assumes that Ad = A*d and r = b - A*x have already been computed. 
% Furthermore, the last neq rows of A correspond to equality conditions in the 
% least squares problem. [Actually, equality rows are not allowed in the current
% implementation].
%
% Note: Unlike theta, this uses the active set of rows to compute the gradient.
%   grad(theta) = - A_I' * A_I * d
% The definition of it at an arbitrary point x is
%   grad(theta) = - A' * (A*x - b)_+
%
% Note: putting in equality rows as two inequalities is a Bad Thang (TM), 
% especially for numerical stability.
%
% For summer 2012: will assume neq = 0, so only ninq = m is allowed.
%
%------------------
% Randall Bramley
% Dept of Computer Science
% Indiana University - Bloomington
%--------------------------------
% Started: 24 August 1993
% Modified: Thu 17 May 2012, 04:46 PM for comments
% Modified: Wed 01 Aug 2012, 12:34 PM comments about equality rows
% Last-Modified: Wed 01 Aug 2012, 12:34 PM
%------------------------------------------------------------------------------
function [val] = dtheta(lambda, Ad, r, neq)

      m    = length(Ad);
      ninq = m - neq;

      % Inequality rows of A:
      z1   = max(lambda*Ad(1:ninq) - r(1:ninq), zeros(ninq,1));
      % Equality rows of A:
      z2   = lambda*Ad(ninq+1:m) - r(ninq+1:m);

      val  = Ad'*[z1;z2];

return
