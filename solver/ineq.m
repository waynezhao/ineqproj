%---------------------------------------------------------------------------------------
%
% An implementation of S.-P. Han's method for solving systems of linear inequalities 
% in a least squares sense. I.e., find an x* that for a given m x n matrix A and an
% m x 1 vector b, solves 
%
%       min || (A*x - b)+ ||
%
% where the trailing + indicates gangster projection onto the nonnegative orthant      
% in m-dimensional space. Or for non-Al Capone fans, u = v_+ is the vector where 
%
%       u(i) = v(i) if v(i) >= 0, and
%       u(i) = 0    if v(i) < 0
%
% The norm is the vector 2-norm. The method first finds a search direction d by
% solving a linear least squares problem with the active rows of A, that is, the
% ones for which the inequalities specified by A*x <= b are violated. Then a line
% search is applied in the direction d, solving the convex and piecewise quadratic
% function of a single variable
%
%       theta(lambda) = || (A(x + lambda*d) - b)+ ||
%                     = || (Ad - r)+ || where r = residual vector of full Ax = b
% 
% for an optimal stepsize lambda. Three methods for finding the search direction and 
% three for finding the stepsize are implemented. This function is a descendent of 
% the one originally done for a SIAM paper, but has been revised to remove the 
% dependence on Matlab's old flops() function. Furthermore it has been saddled with 
% even more stuff, in particular
%
%   a Boolean array J to return the set of active indices from each iteration
%   an array D to return the search direction norms 
%   an array T to return timing information
%
%---------------------------
% Warnings and Limitations:
%---------------------------
%
% 1. This is *not* a high performance implementation, and is instead a tool to 
%       explore the solution algorithm.
%
% 2. It is derived from an earlier implementation, and so the function calls tend 
%       to have more arguments than a well-coded Matlab code should.
%
% 3. The previous implementation counted flops as a metric and so variables like 
%       Ad = A*d were passed as arugments rather than re-computed within 
%       subfunctions. In practice, the matrix*vector ops are cheap when compared 
%       to to the linear equality least squares subproblems each iteration requires. 
%
% 4. This implementation does *not* allow for a mix of inequalities and equalities.
%       The additional code for that obscures some of the algorithm behaviour. 
%       However, in places the indexing variables INEQ and EQ correspond to the 
%       inequalities and equality rows in A. But only EQ = [] is implementated.
%
% 5. Using nargin/nargout or vararg would avoid the hardwired returned values that
%       are irrelevant for some methods. In particular, unless QR with pivoting is
%       chosen, the permutation vector E is not needed.
%
% 6. The Matlab conventions for coding are not followed, e.g., have a brief "help"
%       section separated by a blank line from the rest of the comments, detailed 
%       input checking, and use of all-caps for the help call (e.g., INEQ instead
%       of ineq). In places it violates the Householder convention, e.g., the
%       variable E is a vector, not a matrix. 
%
%-------------------------
% Inputs and parameters: 
%-------------------------
%
%   A          = matrix; all rows are assumed to be for inequalities in this code.
%   x          = initial estimate of solution; x = zeros(n,1) is alright.
%   b          = right hand side vector
%   tolerances = a two entry array with the tolerances on the function/gradient 
%                   norms and the line search, resp.
%   dirmeth    = integer giving method for search direction finding.
%   linemeth   = integer giving method for line search.
%   maxits     = maximum allowed number of iterations.
%   printlevel = amount of printing to perform. Not fully implemented yet, but
%                   * 0: perform limited printing (mainly error checks)
%                   * 1: print out function and gradient data from each step
%                   * 2: print out iteration, line search, and search direction data
%   neq        = number of rows of A corresponding to equality conditions. If
%                   all rows of A are for equalities, then one iteration should 
%                   work but because of floating point vagaries more may be taken.
%                   Currently, only neq = 0 is implemented.
%
%---------
% Ouputs:
%---------
%   x         = putative solution
%   fval      = norm of function value
%   fgradient = norm of gradient
%   errflag   = error code; is nonzero only if d is not a descent direction.
%   J         = Boolean array, one column per iteration, with t/f for active indices
%   D         = vector of norms of search direction d, one value per element
%   T         = timing array;
%                   T(1) = overall time
%                   T(2) = search direction time
%                   T(3) = step size time
%                   T(4) = unused
%   iters     = total number of iterations required
%
%-------------------------------------------------------
% Original version by Randall Bramley and Beata Winnicka
% Dept of Computer Science
% Indiana University - Bloomington
%--------------------------------
% Started: 24   August 1993
% Revised:  1  October 1993
% Revised: 15 November 1993
%
%--------------------------------------------------------------------------------------
% Restarted in summer 2012 with Zachary Johnson, Psychological and Brain Sciences Dept
%--------------------------------------------------------------------------------------
% Started Modifications : Wed 16 May 2012, 08:50 AM 
% Modified: Wed 30 May 2012, 12:48 PM
% Modified: Fri 06 Jul 2012, 10:34 AM J is Boolean, D is vector of norms now
% Modified: Sat 07 Jul 2012, 10:55 AM
% Modified: Fri 27 Jul 2012, 12:59 PM to put comments back
% Modified: Wed 01 Aug 2012, 12:32 PM added more comments, labeled end statements
% Last-Modified: Wed 01 Aug 2012, 03:23 PM
%=======================================================================================

function [x, fval, fgradient, errflag, J, D, T, iters] = ...
    ineq(A, x, b, tolerances, dirmeth, linemeth, maxits, printlevel, neq);

    %------------------------------------------------------------------------------
    % Initialize all the return arguments to something in case of an early return.
    % Otherwise Matlab barfs out.
    %------------------------------------------------------------------------------
    errflag   = 0;
    T         = zeros(1, 4);
    fval      = [];
    fgradient = [];
    J         = false(max(size(A)), maxits); 
    D         = zeros(1, maxits);;
    iters     = 0;
    overall_time0 = clock;
    [m, n] = size(A);
    separator = '-------------------------------------------------------';

    %---------------------------------
    % Number of inequality rows of A:
    %---------------------------------
    ninq   = m - neq;

    %----------------------------------------------------------------
    % Index sets corresponding to inequality and equality rows of A:
    %----------------------------------------------------------------
    INQ    = 1:ninq;
    EQ     = ninq+1:m;
    INQ    = INQ';
    EQ     = EQ';

    %----------------------------------------------------------------------------
    % Note that actually iter should start out at 0, but it is used for indexing
    % into J and D, and so must be positive.
    %----------------------------------------------------------------------------
    iter   = 1;
    r      = b - A*x;
    I      = [find(r(INQ) <= 0); EQ];
    J(I, iter) = true;

    %------------------------------------------------------------------------
    % Find initial active index set, residual, function value and fgradient.
    %------------------------------------------------------------------------
    z = max(-r(INQ),zeros(ninq,1));
    z = [z; -r(EQ)];

    %-----------------
    % Function value:
    %-----------------
    fval = 0.5*z'*z;
    
    %-----------------
    % Gradient value:
    %-----------------
    temp      = A'*z;
    fgradient = temp'*temp;

    if (printlevel > 0)
        disp(sprintf('\n %s', separator)) 
        disp(['Before iteration number ',num2str(iter)]);
        disp(sprintf('   Initial function value is  %e', fval))
        disp(sprintf('   Initial fgradient value is %e', fgradient))
        disp(['   Number of initial active indices: ',num2str(neq+sum(r(INQ)<0))])
    end  % prinlevel > 0

    iters = 0;  % Initialize iteration counter
    E = [1:n]'; % Initialize permutation vector E 
    
    %--------------------
    % Unpack tolerance:
    %--------------------
    epsout  = tolerances(1);
    epsline = tolerances(2);
    
    %-----------------------------------------------------------
    % Begin iterations if fval, fgradient bigger than tolerance
    %-----------------------------------------------------------
    while ((iter <= maxits) && ...
            (fval > epsout) && ...
            (fgradient > epsout))

        %---------------------------------------------------------------
        % Search direction and product with A
        %    dirmeth == 1  use singular value decomposition each time
        %    dirmeth == 2  use QR with pivoting and rank determination
        %    dirmeth == 3  use Matlab backslash 
        %---------------------------------------------------------------

        t0 = clock;
        [d, R, errflag, E] = searchdir(A, I, r, dirmeth, E, printlevel);
        if (errflag ~= 0)
            error('bummed out about search direction')
        end % errflag ~= 0
        t1 = clock;
        timereqd = etime(t1, t0);
        T(2) = T(2) + timereqd;
    
        if (printlevel > 1)
            disp(['SD info: time required was ',num2str(timereqd)])
        end  % printlevel > 1

        %--------------
        % Line search:
        %--------------
    
        t0 = clock;
        [stepsize, fval, linederiv, errflag] = ...
            linesrch(A, x, d, r, fval, epsline, linemeth, neq, printlevel);
        t1 = clock;
        timereqd = etime(t1, t0);
        T(3) = T(3) + timereqd;

        if (errflag ~= 0)
            if (printlevel > 0)
                disp(['LS info: Error in line search; errflag = ',num2str(errflag)])
            end  % printlevel > 0
            return
        end % errflag ~= 0

        if (printlevel > 1)
            disp(['LS info: time required was ',num2str(timereqd)])
        end  % printlevel > 1

        x = x + stepsize*d;    % Update x 
        D(iter) = norm(d);     % Save the norm of d for later mastication
        J(:, iter) = (r <= 0); % paper defines this as .le.

        %-------------------------------------------
        % Compute new quantities for the updated x:
        %-------------------------------------------
        
        iter = iter + 1;
        r    = b - A*x;
        I    = [find(r(INQ) <= 0); EQ];
        z    = max(-r(INQ), zeros(ninq, 1));
        z    = [z; -r(EQ)];
        temp = A'*z;
        fgradient = temp'*temp;

        if (printlevel > 0)
            fval = theta(stepsize, A*d, r);
            disp(sprintf('\n %s', separator)) 
            disp(['Before iteration ',num2str(iter)]);
            disp(sprintf('            function value is %e ',fval))
            disp(sprintf('            fgradient value is %e', fgradient))
            disp(['            active indices: ',num2str(neq+sum(r(INQ)<0))])
        end  % printlevel > 0

%--------------------------------------------------------------------------------
%  RB:
%  6 Jul 2012: Temp for weird case in woohoo.mat
% disp(sprintf('iter %d: fgradient = %e, epsout = %e', iter, fgradient, epsout))
%--------------------------------------------------------------------------------

    end % while (iter <= maxits & fval > epsout & fgradient > epsout)

    %---------------------------------------------------------------------------
    % Why did we stop, anyway? Using if-tests because stop may have been caused
    % by more than one of the conditions being met.
    %---------------------------------------------------------------------------
    if (printlevel > 0)
        if (iter > maxits) 
            disp(sprintf('\nStopping because maxits reached'))
        end
        if (fval <= epsout) 
            disp(sprintf('\nStopping because fval <= epsout'))
        end
        if (fgradient <= epsout)
            disp(sprintf('\nStopping because gradient is small enough'))
        end
        disp(sprintf('\n %s \n', separator))
    end % if (printlevel > 0)

    iters = iter-1;
    overall_time1 = clock;
    T(1) = etime(overall_time1, overall_time0);

    %--------------------------------------------------------------------
    % Truncate J and D to actual number of iterations used, in case they
    % were over-allocated
    %--------------------------------------------------------------------
    D = D(1:iters);
    J = J(:, 1:iters);

return
