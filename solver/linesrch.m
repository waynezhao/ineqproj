%-------------------------------------------------------------------------------
%
% Line search for Han's method for linear inequalities.
%
% Find scalar lambda minimizing theta(lambda) = || (A(x + lambda*d) - b)+ ||,
% where the suffix + indicates gangster projection onto the nonnegative orthant 
% in m-dimensional space. While the search direction is determined only by the
% active inequalities at x, the line search method uses all of the inequalities.
% So the array A here is whole meghillah. 
%
% The line search function theta is convex and piecewise quadratic, making it 
% really easy to minimize. The values of lambda at which two different quadratic 
% intervals stop/start are called "knot points" here. They are the places where
% the ray starting at x and moving in the direction d hits one of the hyperplanes              
% corresponding to one of the inequalities from a row of A. Between knot points,       
% the linesearch function is a quadratic function. After passing through a             
% knot point, the function becomes a different quadratic function. So if lambda 
% is the length moved in the direction d, 
%
%     theta(lambda) =  p*lambda^2 + q*lambda + t,                                      
%
% where (p, q, t) are real numbers. After passing through a knot point, the            
% constants (p, q, t) change to different values, until another knot point is          
% reached. This terminology is borrowed from the mathematical theory of
% interpolation and approximation, but is defined more specifically here.
%
% Because sometimes quadratic interpolation is numerically inaccurate, two of the
% methods use a binary search in the interval [a, b] on the real line. That 
% interval is either provided by searching through the knot points to find which
% interval contains the minimum, or by ignoring the knot points and starting with
% the interval [a, b] = [0, 2].
%
% Note that if the right hand interval endpoint b is ~1, then only 53 steps are
% needed to narrow the interval down to double precision Planck scale: 1.0E-16.
% So binary search is a viable choice. Additionally it seems to be the fastest 
% method and doesn't get stuck with flat quadratic functions and other messy
% situations.
%
%----------
% Inputs: 
%----------
%
%   A = matrix; assumed to consist of all inequalities (i.e., no equality rows).
%
%   x = current iterate
%
%   d = search direction
%
%   r = b - A*x
%
%   fval = 1/2 || (Ax - b)_+ ||^2; function value at current iterate
%
%   epsline = tolerance to use in binary search methods.
%
%   linemeth = method to use; this has values
%
%            => 1  Search knot points until the quadratic interval containing a 
%                   solution is found, then interpolate using the quadratic 
%                   parameters (p, q, t)
%
%            => 2  Perform method 1, but if derivative of theta is bigger than 
%                  epsline, refine the stepsize using binary search
%
%            => 3  Use brute force binary search throughout
%
%   neq = number of equality rows in A; they must be in the last neq rows, 
%           with 0 <= neq <= m. Beware: currently only neq = 0 is supported and
%           tested.
%
%   printlevel specifies whether or not to give progress/info messages.
%           Currently only printlevel > 0 is tested, but later it can be used to
%           provide finer-grained control of the console output.
%
%-----------
% Outputs: 
%-----------
%
%      stepsize is what you think it is
%
%      newfval is function value at stepsize
%
%      newderiv is line search function derivative value at stepsize
%
%      err  = 0 if all's OK
%           = 1 if d is not a search direction
%
%      err  = -k if the k-th input argument is wrong.
%
%---------------------
% Internal variables:
%---------------------
%
%      aval  = the value of theta at the interval's endpoint a = 0
%
%      adval = the value of the derivative of theta at a
%
%      bval  = the value of theta at the interval's endpoint b
%
%      bdval = the value of the derivative of theta at b
%
%      bnode = the interval's right-hand endpoint. This is used instead of 
%               just "b" to avoid confusion with the right-hand side b in the
%               overall inequality least squares solve, and to make it easier to
%               search for using a text editor. bval and bdval are still used for
%               the function and its derivative at bnode = b.
%
%      ind   = indices of knot points that are "well-defined", that is neither
%               NaN nor infinity.
%
% Update: for experiments in summer 2012, will only use inequalities, so neq = 0
% should hold for the experiments. That has been added to the first few lines
% that check the input arguments. When modified, the rows corresponding to 
% equalities probably should be prepended to A, not appended as they were
% earlier.
%
% Additionally: because linemeth = 1 uses both quadratic and binary bisection for
% searching, the code has not been turned into case-switches. That should be done 
% by splitting out binary search as a separate function (eventually).
%
%-----------------
% Randall Bramley
% Dept of Computer Science
% Indiana University - Bloomington
%----------------------------
% Started: 24 August 1993
% Revised 1 October 1993 and countless other times.
% Revised Mon Dec 23 10:01:01 EST 2002
%        to remove flop counts for Matlab 6.X
% Modified: Wed 11 Jul 2012, 12:50 PM
% Modified: Wed 01 Aug 2012, 12:43 PM for comments
% Last-Modified: Wed 01 Aug 2012, 01:19 PM
%-------------------------------------------------------------------------------
function [stepsize, newfval, newderiv, err] = ...
         linesrch(A, x, d, r, fval, epsline, linemeth, neq, printlevel);

    %-----------------------------------------------------------------------
    % Because error condition returns still require all return values to be
    % set, start off with them being nulled:
    %-----------------------------------------------------------------------
    err      = 0;
    stepsize = [];
    newfval  = [];
    newderiv = [];

    %------------------
    % Check the inputs
    %------------------
    [m, n] = size(A);
    if (m < 1 || n < 1)
        err = -1;
    end
    if (neq > 0)
        err = -8;
    end
    if (linemeth < 1 || linemeth > 3)
        err = -7;
    end
    if (err ~= 0)
        return
    end

    %---------------------------------
    % Number of inequality rows of A:
    %---------------------------------
    ninq = m - neq;

    %-------------------------------------------------------
    % Index sets corr to inequality and equality rows of A:
    %-------------------------------------------------------
    INQ = 1:ninq;
    EQ  = ninq+1:m;
    INQ = INQ';
    EQ  = EQ';

    Ad  = A*d;
    err = 0;
    z   = max(-r(INQ),zeros(ninq,1));
    z   = [z;-r(EQ)];

    %------------------------------------------------------------
    % Initialize search interval left hand endpoint to be a = 0:
    %------------------------------------------------------------
    anode = 0;

    %----------------------------------------
    % Function value at lefthand endpoint a:
    %----------------------------------------
    aval = fval;

    %------------------------------------------
    % Derivative value at lefthand endpoint a:
    %------------------------------------------
    adval = Ad'*z;

    %------------------------------------
    % Check if d is a descent direction:
    %------------------------------------
    if (adval > 0) 
        disp('LS info: Error in line search; theta is increasing at 0')
        err = 1;
        error('aaaaaaarrrrrrgggggghhhh!')
        return
    end

    %-----------------------------------------------
    % Handle cases needing quadratic interpolation:
    %-----------------------------------------------
    if (linemeth == 1 | linemeth == 2)

        %-------------------------------------------------------
        % Find knot points which are positive and well-defined:
        %-------------------------------------------------------
        knots = r(INQ)./Ad(INQ);
        ind   = find(~(isnan(knots)) & ~(isinf(knots)) &  knots>0);
        knots = knots(ind);
        numknots = length(knots);
        if (numknots <= 0)
            %-------------------------------------------------------
            % No positive knot points; use [0,2] as search interval
            %-------------------------------------------------------
            bnode = 2;
            %--------------------------------------
            % Find value at right hand endpoint b:
            %--------------------------------------
            z = max(bnode*Ad(INQ)-r(INQ),zeros(ninq,1));
            z = [z;bnode*Ad(EQ)-r(EQ)];
            bval = 0.5*z'*z;
            %------------------------------------------------
            % Perform quadratic interpolation to get minimum
            %------------------------------------------------
            [stepsize,newfval,newderiv,info] = quadint(Ad,r,anode,bnode,aval,bval);
            if (info ~= 0 & printlevel > 0)
                disp(['LS info: *** In quadint, interpolated value set to midpoint of'])
                disp(['LS info: *** search interval since theta(a) > theta(lambda)'])
            end  % if (info ~= 0 & printlevel > 0)
        else
            %------------------------------------------------------------------------
            % Positive knot points exist; search through them to find unimodal a
            % interval. Start with the largest knot point, given by the variable ind:
            %------------------------------------------------------------------------
            knots        = sort(knots);
            [bnode, ind] = max(knots);  % Beware: in general ind and bnode might be
                                        % arrays instead of scalars.
            z = max(knots(ind)*Ad(INQ)-r(INQ), zeros(ninq,1));
            z = [z;knots(ind)*Ad(EQ)-r(EQ)];
            bval = 0.5*z'*z;    %  equivalent to bdval = Ad'*z;
            for i = 1:numknots;
                %----------------------------------------------------------
                % Only need to check knot if it is smaller than current b
                %----------------------------------------------------------
                if (knots(i) < bnode)
                    z = max(knots(i)*Ad(INQ)-r(INQ),zeros(ninq,1));
                    z = [z;knots(i)*Ad(EQ)-r(EQ)];
                    temp = Ad'*z; % the derivative at current knot point

                    if (temp >= 0 & knots(i) < bnode)
                        bnode = knots(i);
                        % bdval = temp;
                        bval = 0.5*z'*z;   
                    end  % temp >= 0 & knots(i) < bnode

                end % knots(i) < bnode

            end  % for i = 1:numknots

        end % if (numknots <= 0)

        %------------------------------------------------------------------------
        % Guard against humongous bnode values by arbitrarily restricting bnode
        %------------------------------------------------------------------------
        if (bnode > 100)
            if (printlevel > 0)
                disp(['LS info: *** bnode in linesrch is ',num2str(bnode)])
                disp(['LS info: ***       bnode is being restricted to 10'])
            end;
            bnode = 10;
            z     = max(bnode*Ad(INQ)-r(INQ),zeros(ninq,1));
            z     = [z;bnode*Ad(EQ)-r(EQ)];
            bval  = 0.5*z'*z;
        end

        %-------------------------------------------------------------------------------------
        % Now [anode,bnode] is an interval on which theta is quadratic; perform interpolation
        %-------------------------------------------------------------------------------------
        [stepsize,newfval,newderiv,info] = quadint(Ad,r,anode,bnode,aval,bval);
        if (info ~= 0 & printlevel > 0)
            disp(['LS info: *** In quadint, interpolated value set to midpoint of'])
            disp(['LS info: ***           search interval since t(a) > t(lambda) '])
        end  % if (info ~= 0 & printlevel > 0)
    end   % if (linemeth == 1 | linemeth == 2)
    
    %---------------------------------------------------------------------------
    % Now if linemeth = 3, perform binary search to get both the initial search
    % interval, and then binary search to find minimum in that interval.
    % If linemeth = 2, check derivative and do second step if it is too large:
    %---------------------------------------------------------------------------
   
    if (linemeth == 3) 
        %-----------------------------------------
        % Initialize search interval to be (0,2].
        %-----------------------------------------
        bnode = 2;
        dprev = max(1,-adval);
        bdval = dtheta(bnode,Ad,r,neq);
        bval  = theta(bnode, Ad, r);
    
        %-----------------------------------------------------------------------
        % If derivative of line search function is negative at bnode,
        % must increase value of bnode to where derivative is nonnegative
        % to be sure to include minimum point in search interval. But limit
        % the size to 256. No sense in being a damned fool about this.
        %-----------------------------------------------------------------------
        if bdval < 0
            kk = 1;
            while (bdval < 0 & kk < 8) % double b until it reaches 2^8 if necessary:

                if (printlevel > 0)
                    disp(['LS info: *** theta is descending at lambda = ',...
                        num2str(bnode)])
                    disp(['LS info: ***    Derivative of theta(bnode) is ',num2str(bdval)])
                    disp('LS info: ***    Upper bound on interval is now doubled')
                end % if (printlevel > 0)

                kk = kk + 1;
                bnode = 2*bnode;
                bdval = dtheta(bnode, Ad, r, neq);
                bval = theta(bnode, Ad, r);
            end  % while bdval < 0 & kk < 8;
        end   % if bdval < 0
    end % if (linemeth == 3) 
    
    %---------------------------------------------------------------------------
    % Completed finding interval in case of linemeth = 3. For linemeth = 2 or 3,
    % perform binary search if needed. For linemeth = 2, just use the value 
    % given by stepsize if it has positive derivative. Otherwise, use bnode.
    %---------------------------------------------------------------------------

    if (linemeth == 2 | linemeth ==3) 
        if (linemeth == 2)
            if (newderiv > 0 )
                bnode = stepsize;
                bdval = newderiv;
            else
                bdval = dtheta(bnode,Ad,r,neq);
            end % if (newderiv > 0 ),
        end % if (linemeth == 2),
    
        %--------------------------------------------------------------------------
        % First check; if derivative at bnode is small enough, just use it as the
        % stepsize. Note that this may not give smallest value of lambda possible.
        %--------------------------------------------------------------------------
        if bdval < epsline;
            stepsize = bnode;
            newfval  = bval;
            newderiv = bdval;
        else  % Must do search inside interval:
  
            %----------------------------------------------------------------
            % Binary search with upper limit of 112 iterations. This
            % is used as a safeguarded approach to finding the stepsize.
            % 2^112 = 5.1923E+33, so that should take care of quad precision.
            %----------------------------------------------------------------
            if (printlevel > 0)
                disp(sprintf('LS info: initial bnode = %e', bnode))
            end;
            kk = 1;
            while bnode - anode > epsline & kk < 112;
                stepsize = (anode + bnode)/2;
                newderiv = dtheta(stepsize,Ad,r,neq);

                if newderiv < 0;
                    anode = stepsize;
                else
                    bnode = stepsize;
                end  % if newderiv < 0

                kk = kk + 1;
            end  % while bnode - anode > epsline & kk < 300

            if (printlevel > 0)
                disp(['LS info: Binary search required ',num2str(kk),' trial values'])
            end;
            newfval = theta(stepsize, Ad, r);
        end  % if (bdval < epsline)
    
    end  % if (linemeth == 2 | linemeth ==3) 

    %---------------------------
    % Display step information
    %---------------------------
    if (printlevel > 0)
        disp(['LS info: Stepsize = ',num2str(stepsize)]);
        disp(['LS info: Search interval width = ',num2str(bnode - anode)]);
        %   disp(['LS info: Distance moved = ',num2str(norm(d)*stepsize)]);
    end % if (printlevel > 0)
    
    %-------------------------------------------------------------------------------
    % Optionally plot the graph of the line search function. Beware that this will
    % draw one for every iteration in ineq.m, so use sparingly. Or better yet, pass
    % in something to indicate when/how the plot should be called.
    %-------------------------------------------------------------------------------
    % plottheta(Ad, r, bnode);
    % title('Plot of theta for current line search')
    % xlabel('lambda')
    % ylabel('theta')
    % disp('Hit return to continue')
    % pause;
    %------------------------------------------------------------------------------
    % Plot the derivative of the line search function. Note that showderiv is a 
    % script, not a function, and it opens its own figure window using positionfig
    %------------------------------------------------------------------------------
    % showderiv;
    % disp('Hit return to continue')
    % pause;
    %--------------------------------------------------------------
    
return
