%-----------------------------------------------------------------------------------
%
% Perform a quadratic interpolation for the minimum of the line search function
% theta. The implementation is not necessarily the best numerically. Another way 
% of doing quadratic interpolation is to use the known derivative values:
%     stepsize = (b*adval-a*bdval)/(adval-bdval);
%
%----------
% Inputs: 
%----------
% a   = left  hand endpoint of quadratic interval [a,b]
% b   = right hand endpoint of quadratic interval [a,b]
% av  = function value at left  hand endpoint; av = theta(a)
% bv  = function value at right hand endpoint; bv = theta(b)
% Ad  = A*d, where d = search direction vector
% r   = residual vector 
%
%-----------
% Outputs: 
%-----------
% lambda = stepsize
% val    = function value at lambda
% fp     = derivative value
% info   = error flag
%
%----------------------
% Internal variables: 
%----------------------
% s      = vector of length 3 containing three points in the interval [a,b] 
% p      = number of points in the interval [a,b] to use for interpolation
% temp   = general purpose scalar
% coeffs = the coefficients of the quadratic q(lambda), where
%       q(lambda) = coeffs(1)*lambda^2 + coeffs(2)*lambda + coeffs(3)
%
% Not named, but the midpoint of the interval [a,b] is used for the third
%   required point in the interval
%
%--------------------------------
% Original code: Randall Bramley
% Dept of Computer Science
% Indiana University - Bloomington
%--------------------------------
% Started: 24 August 1993
% Restarted summer 2012 with Zachary Johnson, Psychological and Brain Sciences Dept
% Modified: Mon 30 Apr 2012, 03:48 PM
% Modified: Wed 01 Aug 2012, 01:28 PM for comments
% Last-Modified: Wed 01 Aug 2012, 01:44 PM
%-----------------------------------------------------------------------------------

function [lambda, val, fp, info] = quadint(Ad, r, a, b, av, bv)
    p      = 3;
    info   = 0;
    s      = [a; 0.5*(a+b); b];
    m      = max(size(Ad));
    temp   = max(s(2)*Ad - r, zeros(m,1));
    vals   = [av; 0.5*sum(temp'.*temp'); bv];
    coeffs = [s.*s, s, ones(size(s))]\vals;
    lambda = -coeffs(2)/(2*coeffs(1));
    temp   = max(lambda*Ad - r, zeros(m,1));
    val    = 0.5*temp'*temp;

    %-------------------------------------------------------
    % As error check, if val is not smaller than that at a,
    % set lambda to be midpoint of interval [a,b].
    %-------------------------------------------------------
    if (val > av)
       info   = 1;
       lambda = s(2);
       temp   = max(lambda*Ad - r, zeros(m,1));
       val    = 0.5*temp'*temp;
    end % if (val > av)

    fp = Ad'*temp;

return
