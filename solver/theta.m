%----------------------------------------------------------------------------------
%
% Evaluate the line search function theta at the point lambda. It is assumed that 
% Ad = A*d and r = b - A*x have been defined by the caller. Or someone else.
%
% Note that the line search function uses the entire matrix A, not just the active
% set of rows. That's because traversing in the search direction may cross knot
% points where another row of A becomes active.
%
% Vectorizing this by letting lambda be a vector just does not work out. For length
% of lambda = p, that requires either a m*p x 1 vector, or a m x p array. Eitherly,
% it's way too much memory. See notebook from 19 June 2012
%
%--------------
% Randall Bramley
% Dept of Computer Science
% Indiana University - Bloomington
%----------------------
% Started: 24 August 1993
% Modified: Wed 13 Jun 2012, 02:25 PM for formatting
% Modified: Wed 20 Jun 2012, 09:29 AM tried vectorizing this function. failed
% Modified: Wed 01 Aug 2012, 12:41 PM for comments
% Last-Modified: Wed 01 Aug 2012, 12:42 PM for comments
%----------------------------------------------------------------------------------

function val = theta(lambda, Ad, r)
      m   = length(Ad);
      s   = max(lambda.*Ad - r, zeros(m, 1));
      val = 0.5*s'*s;
return
