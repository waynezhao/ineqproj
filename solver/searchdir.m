%---------------------------------------------------------------------------------
%
% searchdir uses one of 3 methods to find the search direction for ineq,
% where ineq = "inequality linear least squares". 
%
% I = the index vector for the currently active set of rows of A, so the search 
% direction d solves the linear least squares problem 
%
%       min ||A(I,:)*d - r(I)||
%
% r = b - A*x is a residual vector as would be computed for a regular linear
% least squares problem. For the algorithm, r(I) >= 0 should always hold, 
% although it may be useful in the future to include some indices in I that 
% are within a threshold of being 0. r is of length m, where A is m x n. 
%
% E is the permutation vector (if any) from the QR factorization.
%
% method specifies which linear least squares solver to use to get the 
% search direction d:
%       method = 1: SVD 
%       method = 2: QR with pivoting 
%       method = 3: Matlab's backslash
% [Important: Matlab now has qrinsert() and qrdelete() functions that doubtless
% do the same thing as method 3 in a better way. Need to shift over from the 
% home-brew that Beata wrote to those functions.]
%
% errflag indicates error condition if it is nonzero:
%       errflag = -2 means the index vector I is empty
%       errflag = -5 means the method to use is not valid
%       errflag =  1 means that update or downdate of QR failed
%       errflag =  2 means that update/downdate takes more flops that QR
%       errflag =  3 means function update() encountered a bum size
%
% R is the upper triangular matrix returned by the QR methods, or the diagonal
% matrix for complete orthogonal factorization methods (which includes the SVD).
%
% printlevel, if not 0, prints out information as computation proceeds 
%
% p = min(m, n) for loop control
%
%-----------------------
% Originally written by: 
%-----------------------
% Beata Winnicka
% Dept of Computer Science
% Indiana University - Bloomington
% 1 October 1993 
%------------------------------------------
% Minor hacks afterwards by Randall Bramley
%------------------------------------------
% Modified: Thu Mar 31 16:03:17 EST 1994 To use space saver 
%           version of QR factorization
% Modified: Wed 16 May 2012, 08:22 PM for formatting mostly
% Modified: Wed 01 Aug 2012, 12:16 PM 
%------------------------------------
% Modified by Weiran Zhao afterwards
%------------------------------------
% Modified: Mon,Jul 22th 2013 09:07:17 PM EDT
%           Add parameter 'printlevel', and remove old 'bloviate'
% Last Modified: Mon,Jul 22th 2013 09:49:46 PM EDT
%---------------------------------------------------------------------------------
function [d, R, errflag, E] = searchdir(A, I, r, method, E, printlevel)

    %-------------------------------
    % Check for bad input arguments
    %-------------------------------
    [m, n]  = size(A(I,:));
    errflag = 0;
    d = [];
    R = [];
    if isempty(I)
        errflag = -2;
    end
    if (method < 1  || method > 3)
        errflag = -5;
    end
    if (errflag ~= 0)
        return
    end

%=================================================================================
    
    %---------------------------------------------------
    % Method = 1 uses singular value decomposition
    %---------------------------------------------------
    if (method == 1) 
        if (printlevel~=0)
            disp('SD info: Using SVD')
        end

        [u, s, v] = svd(A(I, :));
        s         = diag(s);
        utr       = u'*r(I);
        y         = zeros(n,1);
        [n1, n2]  = size(I);
        R         = [];

        for kk = 1:min(n, n1)
            if s(kk) > s(1)*max(size(A(I, :))*eps)
                y(kk) = utr(kk)/s(kk);
            else
                if (printlevel~=0)
                    disp(['SD info: Rank deficiency detected; rank = ', ...
                        num2str(kk-1)])
                end
                break
            end
        end
        d = v*y;
        return
    end 

    %------------------------------------------
    % Method = 2 uses QR with column pivoting 
    %------------------------------------------
    if (method == 2)
        if (printlevel~=0)
            disp('SD info: Using QR with colum pivoting');
        end

        [Q, R, E] = qr(A(I, :), 0);
    
        %--------------------------
        % Find numerical rank of R
        %--------------------------
        for p = 1:min(n,m)
            % Should make this a while loop, in case rank < p-1:
            % WTF: why make this claim?
            if abs(R(p,p)) < abs(R(1,1))*max(size(A(I,:))*eps)
               p = p - 1;
               if (printlevel~=0)
                   disp(['SD info: Rank deficiency detected; p = ',num2str(p)])
               end
               break
            end
        end
    
        d = Q'*r(I);
        d = R(1:p,1:p)\d(1:p);
        d = [d;zeros(n-p,1)];
        d(E) = d;  % apply the permutation from QR
        return
    end % of method = 2

    %---------------------------------------------------------
    % Method = 3 uses Matlab backslash
    %---------------------------------------------------------
    if (method == 3)
        if (printlevel~=0)
            disp('SD info: Using Matlab backslash method')
        end
        d = A(I,:)\r(I);
    end % of method == 3

return
