% -----------------------------------------------------------------------------
% Set up a very simple case to check the correctness of ineq solver
% The case is set up as to solve y<=-1*0.9
%                                y>=1*.9
% Which is inconsistent, I expect a solution point on y=0 or around
% 
%---------------------
% Ryan (Weiran) Zhao 
% Computer Science Dept, IU Bloomington
%--------------------------------------------------
% Started: Tue,Jul 23th 2013 02:39:31 PM EDT
% Last Modified: Tue,Jul 23th 2013 03:22:05 PM EDT
% -----------------------------------------------------------------------------
clear all 
close all
clc
%-------------------------------------------
% add solver's path, can call ineq directly
%-------------------------------------------
curDir=pwd;
cd('..');
cd('..');
cd('solver');
parentDir=pwd;
addpath(parentDir);
cd(curDir);

%----------------
% set up A and b
%----------------
A = [0, 1; 
     0, -1];

b = 0.9*[-1;-1];

%--------------------------
% basic parameter settings
%--------------------------
tol=[0.00001,0.00001];
dirmeth = 1;
linemeth = 3;
maxiters = 100;
printlevel = 0;
neq = 0;

%---------------------------------
%solve the problem multiple times
%---------------------------------
times = 10;
m_w = zeros(times,2);
for i=1:times
    w = rand(2,1);
    [w, fval, fgradient, errflag, J, D, T, iters]=...
        ineq(A, w, b, tol, dirmeth, linemeth, maxiters, printlevel, neq)
    m_w(i,:) = w;
end

%--------------
% plot to show
%--------------

figure;
hold on;
plot([-1,1],0.9*[1,1]);
plot([-1,1],0.9*[-1,-1]);
plot(m_w(:,1),m_w(:,2),'r--');
hold off;
