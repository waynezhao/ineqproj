%-------------------------------------------------------------------------
% Time QR factorization of A for num_rep times, report the time in 'time'
%
%-------
% Input
%-------
%       A:  size(A) =[m, n]
%       num_rep: to improve accuracy, time qr(A) for num_rep times
%       timer_to_use: 1 for clock (resolution 10^-6, I guess)
%                     2 for cputime (resolution 10^-3, I guess)
%       print:      print 'A_new = Q*R' if set to 1
%--------
% Output
%--------
%       time:   time to do ONE qr(A);
%---------------------
% Ryan (Weiran) Zhao 
% Computer Science Dept, IU Bloomington
%--------------------------------------------------
% Created: Thu,Jul 25th 2013 11:12:03 AM EDT
% Modified: Thu,Jul 25th 2013 08:10:01 PM EDT
%           add 'print' to indicate whether or not print 'A' to visual check
% Last Modified: Thu,Jul 25th 2013 08:11:26 PM EDT
%-------------------------------------------------------------------------
function time = timeQR(A, num_rep, timer_to_use, print)
    if(length(size(A))~=2) 
        disp('matrix A should be in R^{ m * n}');
        return;
    end
    [m, n] = size(A);
    if(timer_to_use==1)
        t1 = clock;
    else 
        t1 = cputime;
    end
    for i = 1: num_rep
        [Q,R] = qr(A);
    end
    if(timer_to_use==1)
        t2 = clock;
        time = etime(t2,t1)/num_rep;
    else
        t2 = cputime;
        time = (t2-t1)/num_rep;
    end
    if print==1
        A_new = Q*R
    end
end
