%-----------------------------------------------------------------------------
% This function do a comparision between normal QR with qrinsert and qrdelete
% For a matrix A of size m-by-n, to do a QR factorization requires O(mn^2);
% However, to insert or delete one row to or from A requires O(n^2) operations.
%
% In matlab, qrinsert insert ONE row or column to A and then factorize it; while
% qrdelete deletes ONE row or column at a time. There exists method for doing
% a block of insertion or deletion, but matlab does not support it now.
%
% In this function, I compare doing 'num_row_insert' insertion or deletion 
% with doing QR factorization on the new (Augmented or reduced) matrix.
%
%-------
% Input
%-------
%       n_start:    matrix number of column starts from 'n_start'
%       n_step:     column size = n_start + i*n_step, i=0,1,2,...
%       n_max:      maximun column size to test
%       ratio:      row size m = n*ratio; 
%                   for skinny matrix A, ratio > 1 and integer
%                   for square matrix A, ratio = 1
%                   for fat matrix, ratio should be divisible by n_start & n_step
%       num_row_insert:     number of rows to do insertion or deletion
%
%--------
% Output
%--------
%       some figures with 'png' extension should be found in current directory,
%       along with a file 'log', check 'log' to make sure there's no errer 
%       happened when the code was running
%---------------------------------------
% Ryan (Weiran) Zhao 
% Computer Science Dept, IU Bloomington
%--------------------------------------------------
% Created: Fri,Jul 26th 2013 10:03:26 AM EDT
% Modified: Fri,Jul 26th 2013 03:39:39 PM EDT
%           Change from script to function
% Last Modified: Fri,Jul 26th 2013 04:16:22 PM EDT
%-----------------------------------------------------------------------------
function compareQR(n_start, n_step, n_max, ratio, num_row_insert)
    %-------------------------------
    % some parameter you can change
    %-------------------------------
    print = 0;
    reorder = 0;    % reorder index set, for time_downdate
    timer =1;       % 1 for clock, 2 for cputime
    num_rep = 1;
    tol = 1.e-10;   % check if qr factorization is correct

    %---------------------------
    % open a file to keep a log
    %---------------------------
    f_log = fopen('log','a');
    fprintf(f_log, '%s: Experiment with n_start = %d, n_max = %d, n_step = %d\n',...
        datestr(clock,0), n_start, n_max, n_step);

    %----------------------------------------------------
    % storage for qrtime, qr_update and qr_downdate, etc
    %----------------------------------------------------
    qr_time         = zeros(floor((n_max-n_start)/n_step+1),1);
    update_time     = zeros(floor((n_max-n_start)/n_step+1),1);
    downdate_time   = zeros(floor((n_max-n_start)/n_step+1),1);
    insert_time     = zeros(floor((n_max-n_start)/n_step+1),1);
    delete_time     = zeros(floor((n_max-n_start)/n_step+1),1);

    %-----------------------
    % Here is the main loop
    %-----------------------
    for i=n_start:n_step:n_max
        A = rand(ratio*i,i);
        r = rand(num_row_insert, i);
        I = randi(ratio*i*2,num_row_insert,1);
        I = mod(I,ratio*i);
        [I, order] = sort(I,'descend');
        r = r(order,:);
        %------------------------------------------------------
        % A_new is the matrix after 'num_row_insert' insertion
        %------------------------------------------------------
        A_new = A;
        for j=1:length(I)
            A_new = [A_new(1:I(j),:);
            r(j,:);
            A_new(I(j)+1:end,:)];
        end
        
        %--------------
        % start timing
        %--------------
        
        index = (i-n_start)/n_step+1;
        %---------
        % time QR
        %---------
        qr_time1 = timeQR(A,num_rep,timer, print);
        qr_time2 = timeQR(A_new,num_rep,timer, print);
        qr_time(index) = qr_time1+qr_time2;

        %----------------
        % time QR_insert
        %----------------
        [update_time(index), insert_time(index), Up_ret] = ...
            timeUpdate(A,r,I,num_rep,timer,print);

        %----------------
        % time QR_delete
        %----------------
        I = sort(I,'ascend');
        [downdate_time(index), delete_time(index), Dn_ret] = ...
            timeDowndate(A_new,I+1,num_rep,timer,reorder,print);

        %-----------------------------
        % check if we do things right
        %-----------------------------
        if(norm(A-Dn_ret)/norm(A) > tol) 
            fprintf(f_log, 'QR downdate wrong with n = %d\n', i);
        end
        if(norm(A_new-Up_ret)/norm(A_new) > tol) 
            fprintf(f_log, 'QR update wrong with n = %d\n', i);
        end
    end
    %----------------------
    % plot and save result
    %----------------------
    column_width = n_start : n_step : n_max;

    %---------------------------------------
    % normal QR with QR Update and Downdate
    %---------------------------------------
    name1 = sprintf('QRUpDownDateMatrix%d_%d_%dInsertDelete%d',...
        n_start,n_step,n_max,num_row_insert);
    title1 = sprintf('QR Update/Downdate with %d Insert/Delete', num_row_insert);
    h1=figure('visible','off');
    hold on
    plot(column_width,qr_time,'m*--');
    plot(column_width,update_time,'r*--');
    plot(column_width,downdate_time,'b*--');
    legend('Normal QR','QR Update','QR Downdate');
    xlabel('Column size of matrix');
    ylabel('Time in second');
    title(title1);
    hold off
    saveas(h1,name1,'png');

    %-----------------------------------------------
    % time comparison between qrinsert and qrdelete
    %-----------------------------------------------
    name2 = sprintf('QRInsertDeleteMatrix%d_%d_%dInsertDelete%d',...
        n_start,n_step,n_max,num_row_insert);
    title2 = sprintf('QR Insert/Delete with %d Insert/Delete', num_row_insert);
    h2=figure('visible','off');
    hold on
    plot(column_width,insert_time,'b*--');
    plot(column_width,delete_time,'r*--');
    legend('QR insert','QR delete');
    xlabel('Column size of matrix');
    ylabel('Time in second');
    title(title2);
    hold off
    saveas(h2,name2,'png');
end
