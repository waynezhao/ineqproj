%----------------------------------------------------------------------------
% This is a toy example trying to figure out whether qr() and qrinsert() and
% qrdelete() return the same result for same matrix A
%
% Need to add 'Q'and 'R' at the end of each timeQR, timeUpdate, timeDowndate, 
% to the function print out Q, and R; notice there's no ';'
%---------------------------------------
% Ryan (Weiran) Zhao 
% Computer Science Dept, IU Bloomington
%--------------------------------------------------
% Created: Thu,Jul 25th 2013 04:57:10 PM EDT
% Last Modified: Thu,Jul 25th 2013 08:21:30 PM EDT
%----------------------------------------------------------------------------

%-------
% Clean
%-------
clear;
clc;
close all;

%------------------
% parameter set up
%------------------
A = magic(5);
r = ones(2,5);
I = [3,1];
A_new = A;
print = 1;
num_rep = 1;
timer = 1;  % 1 for clock, 2 for cputime
reorder = 0;
for j=1:length(I)
    A_new = [A_new(1:I(j),:);
    r(j,:);
    A_new(I(j)+1:end,:)];
end

disp('============================================================');
qr_time2 = timeQR(A_new,num_rep,timer, print);

disp('============================================================');
[update_time, insert_time] = timeUpdate(A,r,I,num_rep,timer, print);

I = sort(I,'ascend');
disp('============================================================');
[downdate_time, delete_time] = timeDowndate(A_new,I+1,num_rep,...
    timer,reorder,print);
disp('============================================================');
qr_time1 = timeQR(A,num_rep,timer, print);
