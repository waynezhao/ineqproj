%-----------------------------------------------------------------------------
% Time qrdelete() of k rows. Index set I gives the row number to delete from A
%-------
% Input
%-------
%       A:  Matrix A to do qrinsert
%       I:  index set indicates to which should r(i,:) be inserted to A
%       num_rep:    repeat these qrinsert for 'num_rep' to improve accuracy
%       timer_to_use:   1 for clock
%                       2 for cputime
%       reorder:    whether to reorder I, so index won't overflow,
%                   if you for sure I won't overflow this function, set it to 0
%       print:      print A_new = Q*R if this is set to 1
%--------
% Output
%--------
%       time:   time needed to do these operations
%       delete_time: time to do ONE qrdelete, only available in clock time
%       A_new:  A_new is A after insertion
%---------------------
% Ryan (Weiran) Zhao 
% Computer Science Dept, IU Bloomington
%--------------------------------------------------
% Created: Thu,Jul 25th 2013 11:34:52 AM EDT
% Modified: Thu,Jul 25th 2013 03:30:29 PM EDT
%           Add 'delete_time' to report time for doing one qrdelete
%           Add 'reorder' to indicate whether reorder 'I'
% Modified: Thu,Jul 25th 2013 08:08:26 PM EDT
%           Add 'print' to indicate whether print A_new to check
%           Add 'A_new' as return value
% Last Modified: Thu,Jul 25th 2013 08:19:45 PM EDT
%-----------------------------------------------------------------------------
function [time, delete_time, A_new] = ...
    timeDowndate(A,I,num_rep, timer_to_use, reorder, print)
    %-------------
    % Check input
    %-------------
    if(length(size(A))~=2) 
        disp('Matrix A should in R^{m*n}');
        return;
    end

    %-------------------------------------
    % Reorder index I so it is descending
    %-------------------------------------
    if(reorder~=0)
        [m,n] = size(A);
        I = mod(I,m);
        I = sort(I,'descend');
    end

    %-------------
    % start timer
    %-------------
    if(timer_to_use==1)
        t1 = clock;
    else 
        t1 = cputime;
    end

    %-----------------------
    % Here is the core loop
    %-----------------------
    for i = 1: num_rep
        [Q,R]=qr(A);
        t_delete1 = clock;
        for j=1:length(I)
            [Q,R] = qrdelete(Q,R,I(j),'row');
        end
        t_delete2 = clock;
    end

    %-----------
    % end timer
    %-----------
    if(timer_to_use==1)
        t2 = clock;
        time = etime(t2,t1)/num_rep;
    else
        t2 = cputime;
        time = (t2-t1)/num_rep;
    end
    delete_time = etime(t_delete2, t_delete1)/length(I);
    A_new = Q*R;
    if(print==1) 
        A_new
    end
end
