%------------------------
% test for skinny matrix
%------------------------
% insert 1 row 1/16000
compareQR(100,200,2000,8,1)
% insert 4 rows 1/4000
compareQR(100,200,2000,8,4)
% insert 16 rows 1/1000
compareQR(100,200,2000,8,16)
% insert 64 rows 1/250
compareQR(100,200,2000,8,64)
% insert 256 rows 2/125
compareQR(100,200,2000,8,256)

