%-----------------------------------------------------------------------------
% Time qrinsert() of k rows, which are stored in r. Index set I gives the row
% number to which to insert r(i,:) to A
%-------
% Input
%-------
%       A:  Matrix A to do qrinsert
%       r:  number of k rows to be inserted into A
%       I:  index set indicates to which should r(i,:) be inserted to A
%       num_rep:    repeat these qrinsert for 'num_rep' to improve accuracy
%       timer_to_use:   1 for clock
%                       2 for cputime
%       print: print 'A_new' to do visual check if set to 1
%--------
% Output
%--------
%       time:   time needed to do these operations
%       insert_time: average time to do ONE qr_insert; only in clock time
%       A_new:  new matrix after insertion of k rows
%---------------------
% Ryan (Weiran) Zhao 
% Computer Science Dept, IU Bloomington
%--------------------------------------------------
% Created: Thu,Jul 25th 2013 11:34:52 AM EDT
% Modified: Thu,Jul 25th 2013 03:13:46 PM EDT 
%           Report average time to call one qr_insert
% Modified: Thu,Jul 25th 2013 08:13:33 PM EDT
%           Add 'print' to parameter and return 'A_new'
% Last Modified: Thu,Jul 25th 2013 08:19:32 PM EDT
%-----------------------------------------------------------------------------
function [time, insert_time, A_new] = ...
    timeUpdate(A, r, I,num_rep, timer_to_use, print)
    %-------------
    % Check input
    %-------------
    if(length(size(A))~=2) 
        disp('Matrix A should in R^{m*n}');
        return;
    end
    [m,n] = size(r);
    if(m ~=length(I))
        disp('number of rows NOT Equal to number of index');
        return;
    end

    %-------------------------------------
    % Reorder index I so it is descending
    %-------------------------------------
    [m,n] = size(A);
    I = mod(I,m)+1;
    [I,order] = sort(I,'descend');
    r = r(order,:);

    %-------------
    % start timer
    %-------------
    if(timer_to_use==1)
        t1 = clock;
    else 
        t1 = cputime;
    end

    %-----------------------
    % Here is the core loop
    %-----------------------
    for i = 1: num_rep
        [Q,R]=qr(A);
        t_insert1 = clock;
        for j=1:length(I)
            [Q,R] = qrinsert(Q,R,I(j),r(j,:),'row');
        end
        t_insert2 = clock;
    end

    %-----------
    % end timer
    %-----------
    if(timer_to_use==1)
        t2 = clock;
        time = etime(t2,t1)/num_rep;
    else
        t2 = cputime;
        time = (t2-t1)/num_rep;
    end
    insert_time = etime(t_insert2, t_insert1)/length(I);
    A_new = Q*R;
    if(print ==1)
        A_new
    end
end
