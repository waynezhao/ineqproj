%-----------------------------------------------------------------------
%Given w and axis_range_A & B; Plot the slab for triangles separability
%problem
%================================================
%Weiran Zhao, Computer Science Dept, IU Bloomington
%
%Started:   Mon, Mar 18th, 2013 22:40
%------------------------------------------------------------------------
function plotSlab(w,axis_range_A,axis_range_B)

%================================================
%In order to plot the slab, I need to plot
%3 lines, I need to know 2 points of each line
%since I know the coefficient of each line,
%and I want the "line" (segment) to fit in the
%figure, I use 'y_min' and 'y_max' to cut up
%with each of the 3 lines and get the points
%================================================
const=[w(3)-1,w(3),w(3)+1];
clrs=[0.0,0.0,1.0;...
    0.0,0.0,0.0;...
    0.0,1.0,1.0];

y_max=max(axis_range_A(4),axis_range_B(4));
y_min=min(axis_range_A(3),axis_range_B(3));
eps=0.0000001;
linewidth=1.0;
for i=1:3
    if(abs(w(1))<eps)
        disp('value of w(1) is too small, DANGER!');
    end
    x_max=(const(i)-y_max*w(2))/w(1);
    x_min=(const(i)-y_min*w(2))/w(1);
    line([x_min,x_max],[y_min,y_max],...
        'LineWidth',linewidth,...
        'Color',clrs(i,:));
end
end