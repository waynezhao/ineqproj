%----------------------------------------------------------------------
%Given vertices of a triangular, form three inequalities as a criterion
%to test whether a given point is inside or outside of the triangular
%
%Input:     vtx_mat:    vertices matrix, whose row represents a point
%
%Output:    crit_mat:   criterion matrix, whose row represents the coeff
%                       of an inequality in the form of:
%                           a*x+b*y+c <=0
%           axis_range: a rectangular box that tightly wrap the triagular.
%                       It is stored in the format of:
%                           [xmin,xmax,ymin,ymax]
%                       This can specify the range of random number
%                       generator, also can be used as parameters for
%                       'axis' function.
%===================================================
%Weiran Zhao, Computer Science Dept, IU Bloomington
%Started:   10:59 am, Tue, Feb 19th, 2013
%-----------------------------------------------------------------------
function [crit_mat,axis_range]=getCritMat(vtx_mat)

crit_mat=ones(3,3);
axis_range=zeros(1,4);

%three vertices of triangular
va=vtx_mat(1,:);
vb=vtx_mat(2,:);
vc=vtx_mat(3,:);

%set axis range
axis_range(1)=min([va(1),vb(1),vc(1)]);
axis_range(2)=max([va(1),vb(1),vc(1)]);
axis_range(3)=min([va(2),vb(2),vc(2)]);
axis_range(4)=max([va(2),vb(2),vc(2)]);

%set criterion matrix
crit_mat=[vc(2)-va(2),-(vc(1)-va(1)),va(2)*vc(1)-va(1)*vc(2);...
    vb(2)-va(2),-(vb(1)-va(1)),va(2)*vb(1)-va(1)*vb(2);...
    vc(2)-vb(2),-(vc(1)-vb(1)),vb(2)*vc(1)-vb(1)*vc(2)];

%center of triangular
cntr=1/3*(va+vb+vc);
cntr=[cntr,1];

%adjust criterion matrix, so that center point will make it <=0
for i=1:size(crit_mat,1)
    if crit_mat(i,:)*cntr'>0
        crit_mat(i,:)=-crit_mat(i,:);
    end
end

end