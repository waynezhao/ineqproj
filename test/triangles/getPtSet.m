%-------------------------------------------------------------------------
%Given three vertices of a triangle (vtx), Randomly generate no_pt points,
%can choose a fixed seed (I set it to 7) for debugging purpose, otherwise,
%it will shuffle the rng.
%===============================================
%Weiran Zhao, Computer Science Dept, IU Bloomington
%
%Started:   Mon, Mar 18th, 2013, 21:33
%--------------------------------------------------------------------------

function [pt_set, axis_range]=getPtSet(vtx, no_pt, fixed_seed)

if(fixed_seed)
    rng(7)
else
    rng('shuffle');
end

pt_set=ones(no_pt,2);

%Always add vertex a (vtx(1,:)) to point set
pt_set(1,:)=vtx(1,:);

%generate the rest of points 
cnt=2;
[crit_mat,axis_range]=getCritMat(vtx);
while(cnt<=no_pt)
    temp1   =   rand()*(axis_range(2)-axis_range(1))+axis_range(1);
    temp2   =   rand()*(axis_range(4)-axis_range(3))+axis_range(3);
    if isInRange([temp1,temp2],crit_mat)
        pt_set(cnt,:)=[temp1,temp2];
        cnt=cnt+1;
    end
end

end
