%------------------------------------------------------------------------
%Used to test if a point is in triagular defined by criterion matrix.
%   Input:      pt:         point
%               crit_mat:   criterion matrix, check getCritMat for details
%
%   Output:     tf:         true or false, represented by 1 or 0
%===========================================================
%Weiran Zhao, Computer Science Dept, IU Bloomington
%
%Started:   11:35 A.M. Tue, Feb 19th, 2013
%------------------------------------------------------------------------
function tf=isInRange(pt,crit_mat)

new_pt=[pt,1]';
tf=0;
if crit_mat(1,:)*new_pt>0
    return
end
if crit_mat(2,:)*new_pt>0
    return
end
if crit_mat(3,:)*new_pt>0
    return
end
tf=1;
return
end