%-----------------------------------------------------------------------
%Call ineq to solve the triangle separability problem
%Input:
%       A:      matrix represent point set A
%       B:      matrix represent point set B
%       how_w:  ways to generate w
%       fix_seed: fix seed or not
%Output:
%       w:      result
%               with 1 represents all '1's;
%               with 2 represents using rand()
%               with 3 represents using randi();
%               with 4 represents using randn();
%       iters:  iterations used to get result
%=============================================
%Weiran Zhao, Computer Science Dept, IU Bloomington
%
%Started:   Mon, Mar 18th, 2013
%-----------------------------------------------------------------------
function [w, iters]=solveTriangles(A,B,how_w,fix_seed)
%=================================================
%add parent folder, so that to call ineq directly
%=================================================
curDir=pwd;
cd('..');
cd('..');
cd('solver');
parentDir=pwd;
addpath(parentDir);
cd(curDir);

%set seed for rng
if(fix_seed)
    rng(17);
else
    rng('shuffle');
end

%initialize w
switch how_w
    case 1
        w=ones(3,1);
    case 2
        w=rand(3,1);
    case 3
        w=randi(3,1);
    case 4
        w=randn(3,1);
    otherwise
        disp('unknown input for how_w, using all ones');
        w=ones(3,1);
end

%set tolerance
tol=[0.00001,0.00001];
no_A    =   size(A,1);
no_B    =   size(B,1);
G       =   [A, -ones(no_A,1);...
            -B, ones(no_B,1)];
g       =   -[ones(no_A,1);...
            ones(no_B,1)];

%solve the problem
[w, fval, fgradient, errflag, J, D, T, iters]=...
    ineq(G, w, g, tol, 3, 3, 100, 0, 0);
end
