%-------------------------------------------------------------------------
%Given matrix A, B, vtx_A, vtx_B, plot triangles and points within triangles
%===================================================
%Weiran Zhao, Computer Science Dept, IU Bloomington
%
%Started:   Mon, Mar 18th, 2013, 22:44
%-------------------------------------------------------------------------
function plotTriangles(A,B,vtx_A,vtx_B)
linewidth=1.5;
%for matrix A
scatter(A(:,1),A(:,2),'+');
for i=1:size(vtx_A,1)
    next=mod(i,size(vtx_A,1))+1;
    line([vtx_A(i,1),vtx_A(next,1)],...
         [vtx_A(i,2),vtx_A(next,2)],...
         'LineWidth',linewidth,...
         'Color',[1.0 0.0 0.0]);
end
%for matrix B
scatter(B(:,1),B(:,2),'*');
for i=1:size(vtx_B,1)
    next=mod(i,size(vtx_B,1))+1;
    line([vtx_B(i,1),vtx_B(next,1)],...
         [vtx_B(i,2),vtx_B(next,2)],...
         'LineWidth',linewidth,...
         'Color',[0.0 0.0 1.0]);
end
end