%-----------------------------------------------------------------------
%script for testing separability problem of the slab case
%
%Weiran Zhao, Computer Science Dept, IU Bloomington
%
%Started:   Wed, Feb 27th, 2013
%Modified:  Mon, Mar 18th, 2013 22:46 
%           Cut up old code into pieces, add more comments
%-----------------------------------------------------------------------
clear all 
close all
clc
%=================
%parameters
%=================
%whether or not use a fixed seed
fixed_seed  =   false;
%vertex A
vtx_A   =   [-1, 0;...
            -3, 5;...
            -3, -4];
%number of points in A
no_A    =   30;
%vertex B
vtx_B   =   [1, 0;...
             3, 5;...
             3, -5];
%number of points in B
no_B    =   400;

%=================================
%generate data points for set A B
%=================================
[A,axis_range_A]=getPtSet(vtx_A,no_A,fixed_seed);
[B,axis_range_B]=getPtSet(vtx_B,no_B,fixed_seed);

%============================================
%solving this linear seprability problem
%following notation in paper[96]
%============================================
[w,iters]   =   solveTriangles(A,B,1,false);
disp(sprintf('value of gamma is %.8e',w(3)));
disp(sprintf('number of iterations is %d',iters));

%============================================
%Plotting...
%============================================
figure
hold on
%*******************************
%Plot triangles and data points 
%*******************************
plotTriangles(A,B,vtx_A,vtx_B)

%**********************************
%plot slab
%**********************************
plotSlab(w,axis_range_A,axis_range_B)
hold off


